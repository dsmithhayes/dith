var gulp = require('gulp');
var bro = require('gulp-bro');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var jslint = require('gulp-jslint');

gulp.task('build', [ 'lint' ], () => {
  return gulp.src('index.js')
    .pipe(bro())
    .pipe(rename('dith.min.js'))
    .pipe(gulp.dest('public/js/'));
});

gulp.task('lint', () => {
  return gulp.src([ './src/**/*.js', 'index.js' ])
    .pipe(jslint());
});

gulp.task('watch', () => {
    gulp.watch('./src/**/*.js', [ 'lint' ]);
    gulp.watch('./index.js', [ 'lint' ]);
})