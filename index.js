var dom = require('./src/dom.js');
var ajax = require('./src/ajax.js');

window.dom = dom;
window.ajax = ajax;

var d = function (element, options = {}, children = null) {
  var e = document.createElement(element);

  for (name in options) e.setAttribute(name, options[name]);

  if (children) {
    if (children instanceof Array) {
      for (var i = 0; i < children.length; i++) e.appendChild(children[i]);
    } else {
      e.innerHTML = children.toString();
    }
  }

  return e;
};

module.exports = d;
window.d = d;