const express = require('express');
const app = express();

const port = 8080;

let cache = (req, res, next) => {
  res.header('Cache-Control', 'private, no-cache, no-store, must-revalidate');
  res.header('Expires', '-1');
  res.header('Pragma', 'no-cache');
  next();
};

app.get('/list', cache, (req, res) => {
  res.header('Content-Type', 'text/json, application/json');
  res.send(JSON.stringify({
    key: 'value',
    list: [ 1, 2, 3, 4 ],
    obj: { k: 'v' }
  }));
});

app.use(express.static('public'));
app.listen(port, () => console.log('listening at ' + port));