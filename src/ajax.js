module.exports = function (options) {
  var xhr = new XMLHttpRequest();

  if (!xhr) {
    console.log('XHR error.');
    return false;
  }

  if (options.before) before(xhr);

  var successCallback = options.success || null;
  var errorCallback = options.error || null;
  var catchallCallback = options.done || null;

  xhr.onreadystatechange = function () {
    if (xhr.readyState == XMLHttpRequest.DONE) {
      if (xhr.status == 200) {
        if (successCallback) successCallback(xhr);
      } else {
        if (errorCallback) errorCallback(xhr);
      }

      if (catchallCallback) {
        catchallCallback(xhr);
      }
    }
  };

  if (options.headers) {
    for (header in options.headers) {
      xhr.setRequestHeader(header, options.headers[header]);
    }
  }

  if (options.method && options.method == 'POST') {
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
  }

  xhr.open(options.method || 'GET', options.url, options.async || true);

  var dataBuffer;
  if (options.data) {
    dataBuffer = '';
    for (key in options.data) {
      dataBuffer += key + '=' + encodeURIComponent(options.data[key]);
    }
  }

  xhr.send(dataBuffer);
};